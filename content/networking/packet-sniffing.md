+++
title = "Sniffing"
description = ""
categories = []
tags = []
weight = 100
+++

## Packet Sniffing

> Sniffing network traffic packets

### tcpdump

> Dump traffic on a network

```bash
# show interfaces
tcpdump -D

# capture all eth0 with no resolution
tcpdump -n -i eth0

# also display payload
tcpdump -nvX

# use bpf filter
tcpdump "udp and not port 53 and not port 1900"
tcpdump "not arp and not stp and not igmp and not udp and host 192.168.1.1"
```

### tshark

> Dump and analyze network traffic

```bash
# list interfaces
tshark -D

# show all traffic on default interface
tshark

# colourise output
tshark --color

# no lookups and specify an interface
tshark -n -i eth0

# use a filter
tshark -n "host 192.168.10.100 and not arp"

# write packets to file and stdout
tshark -i virbr0 -P -w example.pcap

```

### Extracting objects from pcap files

```bash
# extract all HTTP objects (files, images, etc...)
tshark -r example.pcap --export-objects http,$PWD

# list [ip|tcp|udp] endpoints in a capture file
tshark -q -r example.pcap -z endpoints,ip

# list [ip|tcp|udp] conversations in a capture file
tshark -q -r example.pcap -z conv,ip

# flow by conversation (includes conversation ID)
tshark -r lol.pcap -q -z flow,tcp,standard

# variation to visualise flow
tshark -r lol.pcap -q -z flow,any,standard

# follow tcp conversation ID:2 as ascii output
tshark -r example.pcap -z follow,tcp,ascii,2

# same thing using ip:port notation
tshark -q -r example.pcap -z follow,tcp,ascii,10.0.0.6:20,10.0.0.12:51884
```
