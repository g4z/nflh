+++
title = "Caches"
description = ""
categories = []
tags = []
weight = 100
+++

## Caches

> How to clear various networking caches

```bash
# get DNS cache stats
sudo systemd-resolve --statistics

# flush DNS cache
sudo systemd-resolve --flush-caches

# flush ARP cache
sudo ip -s neigh flush all

```
