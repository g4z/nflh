+++
title = "MAC Address"
description = ""
categories = []
tags = []
weight = 100
+++

## MAC Address

> Information related to network MAC addressing

### macchanger

> Utility for manipulating the MAC address for network interfaces

```bash
# show current mac address
macchanger -s eth0

# set a new mac address
macchanger -a eth0

# set new mac address of the same manufacturer
macchanger -ae eth0

# reconfigure macchanger
sudo dpkg --reconfigure macchanger
```

### ifconfig

> Manually change a mac address

```bash
# bring down the interface
sudo ifconfig eth0 down

# set the new mac address
sudo ifconfig eth0 hw ether 00:28:C7:0A:42:A2

# bring the interface up again
sudo ifconfig eth0 up
```
