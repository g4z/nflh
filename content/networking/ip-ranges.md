+++
title = "IP Ranges"
description = ""
categories = []
tags = []
weight = 100
+++

## IP Ranges

> Information related to IP addressing

### Private ranges

These ranges are reserved by IANA for private intranets and are not routable to the Internet.

| From            | To              | CIDR
| --------------- | --------------- | -----------------
| 10.0.0.0        | 10.255.255.255  | 10.0.0.0 / 8
| 172.16.0.0      | 172.31.255.255  | 172.16.0.0 / 12
| 192.168.0.0     | 192.168.255.255 | 192.168.0.0 / 16

### Reserved ranges

The following ranges are reserved.

| Range           | Description
| --------------- | -----------------------------------------------------------
| 0.0.0.0/8       | Current network (only valid as source address) RFC 1700
| 127.0.0.0       | reserved for loopback and IPC on the localhost.
| 127.0.0.0/8     | loopback IP addresses (refers to self) RFC 5735
| 192.0.0.0/24    | reserved (IANA) RFC 5735
| 192.88.99.0/24  | IPv6 to IPv4 relay. RFC 3068
| 198.18.0.0/15   | network benchmark tests. RFC 2544
| 198.51.100.0/24 | TEST-NET-2. RFC 5737
| 203.0.113.0/24  | TEST-NET-3. RFC 5737
| 224.0.0.0/4     | reserved for multicast addresses. RFC 3171
| 240.0.0.0/4     | reserved (former Class E network) RFC 1700
| 255.255.255.255 | limited broadcast address (to other nodes on  LAN) RFC 919
| 0.0.0.0         | in routing context means the default route RFC 1700
| 0.0.0.0         | in firewall context means "all on local machine" RFC 1700

_NOTE: 255 in any part of the IP is reserved for broadcast addressing_

For additional information, see RFC 1918.
