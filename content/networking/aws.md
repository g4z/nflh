+++
title = "AWS"
description = ""
categories = []
tags = []
weight = 100
+++

## Amazon Web Services

> Various notes and commands

### awscli

#### S3

```bash
# list a bucket
aws s3 ls s3://mybucket/

# upload a file to bucket
aws s3 cp barcode.gif s3://mybucket/barcode.gif
```
