+++
title = "Samba"
description = ""
categories = []
tags = []
weight = 100
+++

## Samba/CIFS

> Commands for working with Windows File Sharing

### smbclient

```bash
# List resources as Administrator user
smbclient -U "Administrator" -L //192.168.56.102

# Access a share interactively
smbclient -U "Administrator" //192.168.56.102/ITDEPT

```
