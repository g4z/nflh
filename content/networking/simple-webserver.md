+++
title = "Simple Webserver"
description = ""
categories = []
tags = []
weight = 100
+++

## Simple Webserver

> One liners to run a temporary web server

```bash
# use python3 to run webserver on 0.0.0.0:8000
python -m http.server

# use python2 to run webserver on 0.0.0.0:8000
python -m SimpleHTTPServer 8000

# use php to run webserver on 0.0.0.0:9999
php -S 0.0.0.0:9999
```
