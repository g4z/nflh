+++
title = "SSH"
description = ""
categories = []
tags = []
weight = 100
+++

## SSH

> OpenSSH SSH client (remote login program)

### Port Forwarding

> Forward ports and create proxies

#### Common options

| Flag | Example                | Description
|------|------------------------|----------------------------------------------
| -L   | -L 9999:example.com:80 | bind to 9999 and forward to example.com:80
| -D   | -D 9999                | bind to 9999 and forward dynamically
| -f   |                        | run the command in the background
| -N   |                        | don't run remote command, only forward ports
| -q   |                        | quiet mode

#### Examples

```bash
# static port forwarding (source port -> target port)
ssh -qfN -L 9999:www.example.com:443 host.com

# dynamic port forwarding (source port -> dynamic port)
ssh -qfN -D 9999 host.com

# test dynamic proxy with curl
curl -sL --socks5 socks5://localhost:9999 https://ifconfig.co
# output is the ipaddr of `host.com`
```

### SSH Configuration

Example of `~/.ssh.config` file...

```bash
Host *
    ServerAliveInterval 60

Host myserver1
    HostName mss2.mil.f0b.burp.net
    Port 2222
    User dave

Host myserver2
    HostName 124.124.124.124
    User susan
    IdentityFile ~/.ssh/myserver2.key
```

### SSHFS

> Filesystem client based on ssh

```bash
# mount home folder of `user` on `host.com` to ~/mnt
sshfs user@host.com: ~/mnt

# mount a specific folder on `host.com` to ~/mnt
sshfs user@host.com:/tmp/dir ~/mnt

# specify a non-default port
sshfs -p 2222 user@host.com: ~/mnt

# umount the directory
sudo umount ./mnt
```

### SCP

> Secure copy (remote file copy program)

#### Common options

| Flag | Example                | Description
|------|------------------------|----------------------------------------------
| -r   |                        | recursively copy tree
| -i   | -i /path/to/priv.key   | specify an alternative SSH private key
| -P   | -P 2222                | specify an alternative remote SSH port
| -l   | -l 4096                | limit bandwidth usage by Kbit/s
| -q   |                        | quiet mode
| -v   |                        | verbose mode

#### Examples

```bash
# copy a local file to homedir on remote host
scp ~/Downloads/something.txt user@host.com:

# recursively copy a local folder to remote host
# this follows symlinks found in the path
scp -r /tmp/data/ user@host.com:/tmp/

# copy single remote file to current directory
scp user@host.com:.ssh/config ~/Downloads/

# manually specify a private key
scp -i ~/.ssh/id_rsa2 user@host.com:example.txt ~/Downloads/

# specify a non-default port
scp -P 2222 user@host.com:example.txt ~/Downloads/

# limit bandwidth (Kbit/s)
scp -l 12345 user@host.com:example.txt ~/Downloads/

# be quiet
scp -q user@host.com:example.txt ~/Downloads/

# be verbose
scp -v user@host.com:example.txt ~/Downloads/
```

