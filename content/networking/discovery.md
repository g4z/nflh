+++
title = "Discovery"
description = ""
categories = []
tags = []
weight = 100
+++

## Discovery

> Finding things on the network

### Host Discovery

```bash
# using ARP to ping a host
arping -c1 192.168.1.100

# using ICMP to ping a host
ping -c1 192.168.1.100
```

### Service Discovery

```bash
# multicast DNS network scan
mdns-scan

# ssdp scan
python ssdp-discoverer.py
```

### Packet Information

The following detail what packets are sent for each method:

#### arping

```bash
arping -c1 192.168.1.5
```

```code
00:e0:4c:xx:xx:xx → ff:ff:ff:ff:ff:ff ARP 42 Who has 192.168.1.5? Tell 192.168.1.97
28:92:4a:xx:xx:xx → 00:e0:4c:xx:xx:xx ARP 60 192.168.1.5 is at 28:92:4a:xx:xx:xx
```

#### ping

```bash
ping -c1 192.168.1.5
```

```code
70.020379866 192.168.1.97 → 192.168.1.5  ICMP 98 Echo (ping) request  id=0x7e52, seq=1/256, ttl=64
70.022005725  192.168.1.5 → 192.168.1.97 ICMP 98 Echo (ping) reply    id=0x7e52, seq=1/256, ttl=255 (request in 4)
```

#### nmap (connect scan)

```bash
nmap -sT 192.168.1.5 -p80
```

```code
192.168.1.97 → 192.168.1.5  TCP 74 35254 → 80 [SYN] Seq=0 Win=29200 Len=0 MSS=1460 SACK_PERM=1 TSval=162202596 TSecr=0 WS=128
 192.168.1.5 → 192.168.1.97 TCP 78 80 → 35254 [SYN, ACK] Seq=0 Ack=1 Win=8688 Len=0 MSS=1460 WS=1 SACK_PERM=1 TSval=84794150 TSecr=162202596
192.168.1.97 → 192.168.1.5  TCP 66 35254 → 80 [ACK] Seq=1 Ack=1 Win=29312 Len=0 TSval=162202598 TSecr=84794150
192.168.1.97 → 192.168.1.5  TCP 66 35254 → 80 [RST, ACK] Seq=1 Ack=1 Win=29312 Len=0 TSval=162202598 TSecr=84794150
```

#### nmap (stealth scan)

```bash
sudo nmap -sS 192.168.1.5 -p80
```

```code
192.168.1.97 → 192.168.1.5  TCP 58 63879 → 80 [SYN] Seq=0 Win=1024 Len=0 MSS=1460
 192.168.1.5 → 192.168.1.97 TCP 60 80 → 63879 [SYN, ACK] Seq=0 Ack=1 Win=8760 Len=0 MSS=1460
192.168.1.97 → 192.168.1.5  TCP 54 63879 → 80 [RST] Seq=1 Win=0 Len=0
```

#### bettercap

```bash
bettercap -eval "syn.scan 192.168.1.5 80"
```

```code
192.168.1.97 → 192.168.1.5  TCP 60 666 → 80 [SYN] Seq=0 Win=0 Len=0
 192.168.1.5 → 192.168.1.97 TCP 60 80 → 666 [SYN, ACK] Seq=0 Ack=1 Win=8760 Len=0 MSS=1460
192.168.1.97 → 192.168.1.5  TCP 54 666 → 80 [RST] Seq=1 Win=0 Len=0
```
