+++
title = "Netcat"
description = ""
categories = []
tags = []
weight = 100
+++

## Netcat

> Arbitrary TCP and UDP connections and listens

`nc` is the original `netcat` command. A modern replacement is `ncat` from the `nmap` package.

#### Common options

| Flag | Example                | Description
|------|------------------------|----------------------------------------------
| -n   |                        | don't resolve any DNS or service names
| -v   |                        | be verbose
| -l   |                        | listen mode
| -m   | -m 1                   | max number of connections allowed
| -u   |                        | enable udp mode (default: tcp)
| -p   | -p 7777                | specify the port
| -e   | -e /bin/bash           | specify a command to run on connect
| -w   | -w 2                   | timeout connection after 2 seconds

### Examples

```bash
# tcp connect to localhost port 7777
ncat localhost 7777

# udp connect to localhost port 7777
ncat -u localhost 7777

# tcp listen on port 7777
ncat -lvp 7777 localhost

# udp listen on port 7777
ncat -lvup 7777 localhost

# listen port 7777 and run /bin/bash on connect
ncat -lnvp 7777 -e /bin/bash
```
