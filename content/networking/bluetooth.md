+++
title = "Bluetooth"
description = ""
categories = []
tags = []
weight = 100
+++

## Bluetooth

> Various commands related to Bluetooth and BluetoothLE

```bash
# list bluetooth interfaces
hciconfig

# bring interfaces up or down
sudo hciconfig hci0 up
sudo hciconfig hci0 down

# list local BT devices
hcitool dev

# scan for remote BT devices
sudo hcitool scan

# list paired devices
bt-device -l

# connect and pair a device
sudo bt-device -c <macaddr>

# find the service to connect to
sdptool browse <macaddr>
```
