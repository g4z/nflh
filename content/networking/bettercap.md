+++
title = "Bettercap"
description = ""
categories = []
tags = []
weight = 100
+++

## Bettercap

> Swiss Army knife for WiFi, Bluetooth Low Energy, wireless HID hijacking and Ethernet networks reconnaissance and MITM attacks.

### Basics

```bash
# startup and run the commands
bettercap -eval "ticker on"

# startup and run a caplet
bettercap -caplet /path/to/caplet.bcap

# startup with options
bettercap -iface eth0 -no-colors -no-history
```

### Command (caplet) syntax

#### Basics

```bash
# print all variables
get *

# print some variables
get net.*

# clear the screen
clear
```

#### REST API

```bash
# configure the rest api
set api.rest.address 0.0.0.0
set api.rest.port 8081
set api.rest.websocket true
set api.rest.refpolicy ""

# enable the rest api
api.rest on

# disable the rest api
api.rest off
```

### Net Module

```bash
# enable active discovery
net.probe on

# configure net sniffer
set net.sniff.verbose false
set net.sniff.local true
set net.sniff.filter tcp port 443

# enable sniffer
net.sniff on

# configure proxy scripts
set https.proxy.script example.js
set http.proxy.script example.js

# enable the proxies
http.proxy on
https.proxy on

# configure arp spoofing
set arp.spoof.targets 192.168.1.100
set arp.spoof.whitelist 192.168.1.2,192.168.1.3
set arp.spoof.internal true

# enable arp spoofing
arp.spoof on

# ignore certain events (less verbose output)
events.ignore endpoint.new
events.ignore endpoint.lost

# configure dns spoofing
set dns.spoof.domains perugiatoday.it
set dns.spoof.address 8.8.8.8

# enable dns spoofing
dns.spoof on

# quick hack inject javascript
set http.proxy.injectjs "alert(123);"
set https.proxy.injectjs "alert(123);"
```

### Build bettercap for ARM

```bash
# download libpcap source
cd ~/src
curl -sLO http://www.tcpdump.org/release/libpcap-1.8.1.tar.gz
tar zxf libpcap-1.8.1.tar.gz
cd libpcap-1.8.1

# configure and compile libpcap
CC=arm-linux-gnueabihf-gcc \
    ./configure \
    --host=arm-linux \
    --with-pcap=linux \
    && make

cd ~/src/bettercap

CC=arm-linux-gnueabihf-gcc \
CGO_ENABLED=1 \
GOARCH=arm \
GOOS=linux \
CGO_LDFLAGS="-L$HOME/src/libpcap-1.8.1" \
go build \
-o bettercap \
$(pwd)
```
