+++
title = "Wireguard"
description = ""
categories = []
tags = []
weight = 100
+++

## Wireguard

```bash
# generate private key
wg genkey > private.key

# generate public key
wg pubkey < private.key

# bring up an interface
ip link add wg0 type wireguard

# set tunnel ip addr
ip addr add 10.0.0.1/24 dev wg0

# set the key for the tunnel
wg set wg0 private-key private.key

# bring up the interface
ip link set wg0 up

# show current information
wg

# add the public key of a peer
# 10.0.0.2/32 is the peer IP within the tunnel
# 192.168.1.2:51820 is the network addr of the peer
wg set wg0 peer <peer-public-key> allowed-ips 10.0.0.2/32 endpoint 192.168.1.2:51820
```
