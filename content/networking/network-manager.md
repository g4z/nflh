+++
title = "Network Manager"
description = ""
categories = []
tags = []
weight = 100
+++

## Network Manager

> Network management daemon

```bash
# restart network-manager
sudo service network-manager restart
```

### nmcli

> Command-line tool for controlling NetworkManager

```bash
# show connections
sudo nmcli connection show

# activate a connection
sudo nmcli connection up supernova

# deactivate a connection
sudo nmcli connection down supernova

# TODO finish me

nmcli c show
nmcli c reload supernova
sudo nmcli c reload supernova
ping google.com
sudo nmcli c down supernova
sudo nmcli c up supernova

```
