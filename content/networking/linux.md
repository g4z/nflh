+++
title = "Linux"
description = ""
categories = []
tags = []
weight = 100
+++

## Linux

> Various Linux networking tools

### ip

> Show and manipulate routing, network devices, interfaces and tunnels

```bash
ip addr show dev eth0
ip addr -i eth0
ip link delete tap0
ip link set dev tap0 up
```

### route

> Show and manipulate the IP routing table

```bash
route -n
```

### arp

> Manipulate the system ARP cache

```bash
# list known hosts (no resolution)
arp -n
```
