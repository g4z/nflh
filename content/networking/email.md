+++
title = "Email"
description = ""
categories = []
tags = []
weight = 100
+++

## Email

> Related to send and receive email

### Sendmail manually

```bash
# connect smtp using telnet
telnet smtp.sendgrid.net 2525

# enter auth command
AUTH LOGIN

# enter b64 encoded username
YXBpa4V5

# enter b64 encoded password
U0cuaU9MbW0xVzVSZTJzZGd3ZVk3ZFY0US5LbHBpckRGQUZCZFIyMWxsa2RDb1MzVDFiZ1N2RFBRM3VnV254YjdWMlNv

# commands to create email
MAIL FROM:sender@domain1.com
RCPT TO:receiver@domain2.com
DATA
FROM:Testy Tester <sender@domain1.com>
SUBJECT:This is a test
hello

this is a test.

goodbye.

.
quit
```
