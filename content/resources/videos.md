+++
title = "Video"
description = ""
categories = []
tags = []
weight = 100
+++

## Video

> A list of interesting videos

- [Golang UK Conference 2017 | Arne Claus - Concurrency Patterns in Go](https://www.youtube.com/watch?v=Lxt8Vqn4JiQ)
- [Golang UK Conference 2017 | Filippo Valsorda - Fighting Latency](https://www.youtube.com/watch?v=rDRa23k70CU)
- [Francesc Campoy | Go Tooling in Action](https://www.youtube.com/watch?v=uBjoTxosSys)
- [Release Party #GoSF | Peter Bourgon - Ways To Do Things](https://www.youtube.com/watch?v=LHe1Cb_Ud_M)
- [AppSec California 2016 | Samy Kamkar - Radio Hacking: Cars, Hardware, and more!](https://www.youtube.com/watch?v=1RipwqJG50c)
- [Hackaday Superconference 2017 | Samy Kamkar - Creating Vehicle Reconnaissance & Attack Tools](https://www.youtube.com/watch?v=RpD-yMcg4P4)
- [Defcon 18 | Samy Kamkar - How I met your girlfriend](https://www.youtube.com/watch?v=YDW7kobM6Ik)
