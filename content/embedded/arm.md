+++
title = "ARM"
description = ""
categories = []
tags = []
weight = 100
+++

## ARM

> A family of RISC architectures

### Run ARM binary on x64

```bash
# run ARM binary on x64
qemu-arm /path/to/bin
```

### ARM chroot on x64

> Working with an ARM chroot environment on Linux x64

```bash
# This provides the qemu-arm-static binary
apt-get install qemu-user-static

# Mount my target filesystem on /mnt
mount -o loop fs.img /mnt

# Copy the static ARM binary that provides emulation
cp $(which qemu-arm-static) /mnt/usr/bin
# Or, more simply: cp /usr/bin/qemu-arm-static /mnt/usr/bin

# Finally chroot into /mnt, then run 'qemu-arm-static bash'
# This chroots; runs the emulator; and the emulator runs bash
chroot /mnt qemu-arm-static /bin/bash
```

### References

- [how-can-i-chroot-into-a-filesystem-with-a-different-architechture](https://unix.stackexchange.com/questions/41889/how-can-i-chroot-into-a-filesystem-with-a-different-architechture)
