+++
title = "Atmel AVR®"
description = ""
categories = []
tags = []
weight = 100
+++

## Atmel AVR®

> Commands for working with Atmel AVR® chips

```bash
# list valid programmer options
avrdude -c ?

# list valid parts
avrdude -p ?

# write hex file
avrdude -p m328p -c usbasp -P usb -U flash:w:/pth/to/project.hex
```
