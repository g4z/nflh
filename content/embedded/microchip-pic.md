+++
title = "Microchip PIC®"
description = ""
categories = []
tags = []
weight = 100
+++

## Microchip PIC®

> Information for working with Microchip PIC® chips

### Microchip MPLAB®

> Notes for installing Microchip MPLAB

```bash

# MPLABX works with Pickit2
# CLI/pk2cmd method (seems flakey!!)
# You are using systemd.
# MPLAB X IDE and IPE use systemd to handle USB plug and play events. They do this by using sockets as an interprocess communications mechanism.
# Please make sure that interprocess communications under systemd are allowed in the local host. Some Linux distributions do not allow interprocess communications. For example, if the following option is set in the systemd-udevd unit configuration file:
#    IPAddressDeny=any
# then MPLAB X communications library will not be able to handle plug and play events. You might need to create an override file containing this option:
#    IPAddressAllow=localhost

```

### Pk2cmd

> Microchips old programming tool

Note that from my experience with `pk2cmd` and the Pickit2, this method is not very reliable. Constant unplugging of device because pk2cmd "loses" the Pickit2 programming. Better off using MPLAB.

```bash
# Check support chips
# pk2cmd -?P | grep 628

# Write hex file
pk2cmd -PPIC16F628A -M -F/home/gareth/MPLABXProjects/test3.X/dist/default/production/test3.X.production.hex

# Verify hex file is the one written to the chip
pk2cmd -PPIC16F628A -Y -F/home/gareth/MPLABXProjects/test3.X/dist/default/production/test3.X.production.hex

# Run program on chip
pk2cmd -PPIC16F628A -A5 -T

# Stop the program running
pk2cmd -PPIC16F628A -R
```


## Links

- [microchip.com](https://www.microchip.com/)
- [microchip-pic-devices](https://www.microchip.com/design-centers/8-bit/pic-mcus/device-selection)
- [microchip-mplab-ide](https://www.microchip.com/mplab/mplab-x-ide)
- [program-pic-with-pickit2-linux-cli](http://curuxa.org/en/Program_PICs_with_a_PICkit2_using_the_command_line_on_Linux)