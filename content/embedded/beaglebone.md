+++
title = "Beaglebone"
description = ""
categories = []
tags = []
weight = 100
+++

## Beaglebone

> Commands for working with the Beaglebone

```bash
# To turn these images into eMMC flasher images, edit the /boot/uEnv.txt file
# on the Linux partition on the microSD card and remove the '#' on the line
# with 'cmdline=init=/opt/scripts/tools/eMMC/init-eMMC-flasher-v3.sh'.
# Enabling this will cause booting the microSD card to flash the eMMC. Images
# are no longer provided here for this to avoid people accidentally overwriting
# their eMMC flash.
#
#

```

### Links

- [beagleboard.org](https://beagleboard.org/)
- [beaglebone-black](https://beagleboard.org/black)
- [latest-images](https://beagleboard.org/latest-images)
