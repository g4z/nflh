+++
title = "Raspberry PI"
description = ""
categories = []
tags = []
weight = 100
+++

## Raspberry PI

> Commands for working with the Raspberry PI

### Emulation

```bash
# ================================================
# Use qemu to run RPi (ARM) emulator on Ubuntu X64
# ================================================
#
# 1) Download image from here:
#    https://www.raspberrypi.org/downloads/raspbian/
#
# 2) Download RPi QEMU kernel from here:
#    wget https://github.com/dhruvvyas90/qemu-rpi-kernel/raw/master/kernel-qemu-4.4.34-jessie
#
# 3) Follow these instructions:
#    https://github.com/dhruvvyas90/qemu-rpi-kernel/wiki/Emulating-Jessie-image-with-4.x.xx-kernel
#    https://blog.agchapman.com/using-qemu-to-emulate-a-raspberry-pi/
#
# 4) Use this script to start the emulator
#

#
# Setup networking
#
# =========(Option 1)=========
#
# This creates a bridge with IP addr from DHCP and connects
# the host's main interface to it and everything else to it.
#
# Create a bridge and attach main host interface to it
# then create a TAP device and attach that to the bridge
# then run dhcp client on the bridge
#
# brctl addbr br0
# ip addr flush dev enx00e04c6804a7
# brctl addif br0 enx00e04c6804a7
# tunctl -u $(whoami)
# brctl addif br0 tap0
# ip link set dev br0 up
# ip link set dev tap0 up
# dhclient br0
#
#
# =========(Option 2)=========
#
# Use existing QEMU/KVM bridged called virbr0
#
# tunctl -u $(whoami)               # create tap0 device
# sudo brctl addif virbr0 tap0      # connect it to virbr0
# ip link set dev tap0 up           # activate tap0
#


# run the pi
qemu-system-arm \
    -kernel kernel-qemu-4.4.34-jessie \
    -append "root=/dev/sda2 panic=1 rootfstype=ext4 rw" \
    -hda 2018-11-13-raspbian-stretch-lite.qcow \
    -cpu arm1176 \
    -m 256 \
    -M versatilepb \
    -no-reboot \
    -serial stdio \
    -net nic \
    -net tap,ifname=tap0,script=no,downscript=no

#append = init=/bin/bash
#-net user \


## cleanup (Option 1) network setup
# pkill "dhclient br0"
# ip link set dev tap0 down
# ip link set dev br0 down
# brctl delif br0 enx00e04c6804a7
# brctl delif br0 tap0
# brctl delbr br0
# ip link delete tap0
# ip link delete br0

## cleanup (Option 2) network setup
# ip link set dev tap0 down
# brctl delif virbr0 tap0
# ip link delete tap0

```
