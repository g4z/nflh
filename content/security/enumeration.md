+++
title = "Enumeration"
description = ""
categories = []
tags = []
weight = 100
+++

## Enumeration

> Enumerate all the things

### User environment

```bash
# list available sudo commands
sudo -l

# list cronjobs
crontab -l

# what they been doin?
cat ~/.bash_history

```

### URI

> Enumerating URI paths

### wfuzz

> A web application bruteforcer

```bash
# dirb like path finder
wfuzz -z file,directory-list-1.0.txt --hc 404 -u http://localhost/FUZZ

# fuzz an api
wfuzz -z file,entities.txt -z file,actions.txt --hc 404 -u http://localhost/api/v1/FUZZ/FUZ2Z

```

### dirb

> Web Content Scanner

```bash
# use default wordlist: .../dirb/wordlists/common.txt
dirb http://localhost/

# specify a wordlist
dirb http://localhost/ ~/wordlists/custom.txt
```
