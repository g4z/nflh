+++
title = "Chromecast"
description = ""
categories = []
tags = []
weight = 100
+++

## Chromecast

> Tools for playing with Chromecast

### catt

```bash
# find Chromecast devices on the network
# example output...
# Scanning Chromecasts...
# 192.168.11.16 - Living Room - Google Inc. Chromecast
catt scan

# show currently playing information
catt -d "Living Room" status

# play a video
catt -d "Living Room" cast "https://www.youtube.com/watch?v=3MAqlEMITzw"

# control the current playing video
catt -d "Living Room" pause|play|stop

```
