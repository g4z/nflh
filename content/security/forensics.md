+++
title = "Forensics"
description = ""
categories = []
tags = []
weight = 100
+++

## Forensics

> Gathering data about things and stuff

### Images

> Analysing meta information in image files

#### Exiftool

> Read and write meta information in files

```bash
# display all exif data
exiftool example.jpg

# remove all exif data
exiftool -all= example.jpg

```

### Disks/Images

```bash
# show information about a disk image
disktype /path/to/example.img

# show information about a physical disk
sudo disktype /dev/sda3 
```
