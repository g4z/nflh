+++
title = "GPG"
description = ""
categories = []
tags = []
weight = 100
+++

## GPG

> GPG cryptography commands

```bash
# list public keys in keychain
gpg -k

# list private keys in keychain
gpg -K

# import a public key to my keychain
gpg --import some.key

# encrypt a file using my private key
gpg -e -r <MY_KEY_ID> example.txt

# decrypt a file using a known private key (autofound)
gpg -d example.txt.gpg

# have to trust to encrypt using an untrusted public key
gpg --trust-model always -e -r <KEY_ID> example.txt

# export a public key as ascii
gpg --armour --export <KEY_ID>

# export secret keys as ascii (for testing)
gpg --armor --export-secret-keys
```
