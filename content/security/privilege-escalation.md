+++
title = "Privilege Escalation"
description = ""
categories = []
tags = []
weight = 100
+++

## Privilege Escalation

> Commands for getting elevated privileges

### Running commands with various tools

### find

```bash
# execute commands with find
find /etc/hostname -exec /bin/bash \;
```

### awk

```bash
# run system commands with awk
awk 'BEGIN{system("whoami")}'
```
