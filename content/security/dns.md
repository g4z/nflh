+++
title = "DNS"
description = ""
categories = []
tags = []
weight = 100
+++

## DNS

> Tools for DNS

### zone trasfer

```bash
dig axfr rawstudios.ca @ns12.dnsmadeeasy.com
```

### amass

```bash
# enumerate dns records with ipaddr
amass -ip -d host.com
```
