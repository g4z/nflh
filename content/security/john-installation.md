+++
title = "John (Install)"
description = ""
categories = []
tags = []
weight = 100
+++

## John the Ripper

> Active password cracking tool

### Installation

The `john` in the Ubuntu repositories is a single-core minimal version. To get all features, you should install the `jumbo` version.

```bash
git clone git://github.com/magnumripper/JohnTheRipper -b bleeding-jumbo ~/src/john
cd ~/src/john
./configure
make -s clean
make -sj4
```

If the build failed, see the INSTALL* files in the `docs` folder for help.

#### Testing

John can run a series of tests so you can make sure everything is working and get an idea of how fast it runs on your system. If you check the CPU usage (using `htop` for example), you should see that all cores are active during the test.

```
cd ~/srv/john/run
./john --test
```

This will run various benchmark tasks using all available CPU cores.

At this point, you can run John from the build directory directly without needing to install system-wide. If you want to install John system-wide, please see the documentation folder.
