+++
title = "Reversing"
description = ""
categories = []
tags = []
weight = 100
+++

## Reversing

### Tools

> Common Linux tools for examining binaries

```bash
# show basic information like:
# ./main: ELF 64-bit LSB executable, x86-64, version 1 (SYSV), statically linked, not stripped
file /path/to/bin

# extract strings longer than 10 characters from binary
strings -10 /path/to/bin

# disassemble to stdout
objdump -Mintel -D /path/to/bin

# output ELF headers
readelf -h /path/to/bin

# create a hexdump of the binary
xxd /path/to/bin

# show linked libraries
ldd /path/to/bin
```

### XXD

> Make a hexdump (or do the reverse)

```bash
# 2 byte hex (big endian) and character display
echo -n "example" | xxd

# 2 byte hex (little endian) and character display
echo -n "example" | xxd -e -g2

# plain (postscript) hex display
echo -n "example" | xxd -p

# reverse the process above
echo -n "6578616d706c65" | xxd -p -r
```

### Hexdump

> ASCII, decimal, hexadecimal, octal dump

```bash
# 1 byte character display
echo -n "example" | hexdump -c

# 1 byte hex and character display
echo -n "example" | hexdump -c

# 2 byte hex display
echo -n "example" | hexdump -x
```

### Binwalk

> Tool for searching binary images for embedded files and executable code

```bash
# examine file
binwalk /path/to/bin

# extract all objects
binwalk --dd '.*' /path/to/bin
```

### Steghide

> A steganography program

```bash
# try extract hidden data from media file
# <blank password>
steghide extract -sf example.jpg
```

### Radin2

> Binary program info extractor

```bash
# show binfile info
rabin2 -I /path/to/bin

# list all found symbols
rabin2 -s /path/to/bin

# list exported symbols
rabin2 -E /path/to/bin

# list sections
rabin2 -S /path/to/bin
```

### Radare2

> Advanced commandline hexadecimal editor, disassembler and debugger

```bash
radare2 ./main
```

### Registers

> About the CPU registers

| Register | Description
|----------|---------------------------------
| rip      | instruction pointer
| rax      | return values
| rbx      | general purpose
| rcx      | general purpose
| rdx      | general purpose
| rsp      | stack pointer (top)
| rbp      | stack base pointer (bottom)
| rsi      | source index
| rdi      | destination index
| r8-r15   | general purpose

You can address different parts of a register:

```code
0x1122334455667788
  ================ rax (64 bits)
          ======== eax (32 bits)
              ====  ax (16 bits)
              ==    ah (8 bits)
                ==  al (8 bits)
```

For syscalls, registers are used as follows:

| Register | Description
|----------|-----------------
| rax      | return value
| rdi      | arg0
| rsi      | arg1
| rdx      | arg2
| rdx      | arg3
| r8-r10   | argN

### Assembly Instructions

> Intel vs AT&T syntax

Instruction formats for each are asa follows:

- Intel: &lt;inst&gt; &lt;dst&gt;,&lt;src&gt;
- AT&T:  &lt;inst&gt; &lt;src&gt;,&lt;dst&gt;

Example of the differences between syntaxes:

```
# Intel syntax example
mov rbp, rsp
mov edi, 0x4005e4

# AT&T syntax example
mov %rsp,      %rbp
mov $0x4005e4, %edi
```

### Links

- [Intro to Reverse Engineering and Debugging](https://docs.google.com/presentation/d/1vJWsVZnpD25jqLQWeLvDXZSD2MMM5_tyBAqfWnaIx-c)
- [Video: Lecture for above](https://www.youtube.com/watch?v=LAkYW5ixvhg)
