+++
title = "Filesystem"
description = ""
categories = []
tags = []
weight = 100
+++

## Filesystem

> Tools for examining the filesystem

### mimetype/encoding

Determine file mime and encoding information

```bash
file -b --mime-type /etc/passwd
# output: text/plain

file -b --mime-encoding /etc/passwd
# output: us-ascii

file -ib /etc/passwd
# output: text/plain; charset=us-ascii

encguess /etc/passwd
# output /etc/passwd     US-ASCII
```

Convert between encodings

```bash
# convert UTF8 encoded file to ASCII
cat utf8.txt | iconv -f UTF-8 -t ASCII

# convert ISO-8859-1 file into UTF-8
cat latin1.txt | iconv -f latin1 -t UTF-8
```

### find

```bash
# find all setuid binaries
find / -perm -u=s -type f

# find world writable files/dirs
find / -type f -perm -o=w
find / -type d -writable

# find files >1MB and <10MB
find -size +1M -a -size -10M

# using piped commands with exec
find -type f -exec file --mime {} \+ | grep binary

```
