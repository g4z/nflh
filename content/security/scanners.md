+++
title = "Scanners"
description = ""
categories = []
tags = []
weight = 100
+++

## Scanners

> Tools for scanning the things

### WPScan

_NOTE: On Ubunut 18.04, installing ruby2.6 from the snap caused me errors in loading shared libraries (eg. libcurl). I removed the snap and installed ruby2.5 from the repository... which worked fine_

```bash
# update the wpscan vuln. database
wpscan --update

# perform a scan against a site
wpscan --url www.example.com/wordpress/

# perform brute force login attempt with wordlist
wpscan --url www.example.com --wordlist mylist.lst --threads 25

# perform brute force login for user admin only
wpscan --url www.example.com --wordlist mylist.lst --username admin
```
