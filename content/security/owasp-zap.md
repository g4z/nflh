+++
title = "OWASP ZAP"
description = ""
categories = []
tags = []
weight = 100
+++

## OWASP Zed Attack Proxy

> An open-source web application security scanner

### Using the docker images

```bash
# run a `quick scan` using the weekly docker image
docker run -i owasp/zap2docker-weekly zap-cli quick-scan \
    --self-contained \
    --start-options '-config api.disablekey=true' \
    http://localhost

# run a `baseline scan` using the weekly docker image
docker run -i owasp/zap2docker-weekly zap-baseline.py -t http://localhost
```

### zap-cli

> A simple tool for interacting with OWASP ZAP from the commandline

```bash
# example running with zap-cli
zap-cli --log-path /tmp --api-key=XXXXXX --zap-path /opt/ZAP --port 8080 --verbose start
```
