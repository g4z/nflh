+++
title = "NMAP"
description = ""
categories = []
tags = []
weight = 100
+++

## NMAP

### Command line options

#### Common options

| Flag | Description
|------|-------------------------------------------------------------
| -A   | OS and version detection, script scanning, and traceroute
| -F   | Scan fewer ports for a faster scan
| -v   | verbose mode
| -n   | don't resolve any DNS or service names

#### Discovery options

| Flag | Description
|------|-----------------------------------------------------------
| -Pn  | Treat all hosts as online -- skip host discovery
| -PS  | TCP SYN discovery method
| -PA  | TCP ACK discovery method
| -PU  | UDP discovery method
| -PY  | SCTP discovery method
| -PE  | ICMP discovery method
| -PP  | Timestamp discovery method
| -PM  | Netmask discovery method
| -PO  | IP Protocol Ping
| -PR  | ARP Ping

#### Scan options

| Flag | Description
|------|-----------------------------------------------------------
| -sL  | List Scan - simply list targets to scan
| -sS  | TCP SYN scan
| -sT  | TCP connect scan
| -sA  | TCP ACK scan
| -sU  | UDP scan
| -sN  | TCP Null scan
| -sF  | FIN scan
| -sX  | Xmas scan
| -sO  | IP protocol scan

### Discovery examples

```bash
TODO: finish me
```

### Scan examples

```bash
# quick tcp scan
nmap -F host.com

# same with sevice version scan
nmap -sV -sT host.com

# verbose scan top 1000 ports
nmap -A host.com

# scan all ports in fast mode
nmap -T insane -p1-65535 host.com

# specify specific ports
nmap -sT -p U:53,111,137,T:21-25,80 host.com

# scan top 10 ports
nmap --top-ports 10 host.com

# randomise mac address on nmap scan
nmap -sT -p80,8080 --spoof-mac ca:5f:23:ba:94:99 host.com

# check if a webhost has HSTS enabled
nmap -p 443 --script http-hsts-verify secure.hosts.com

```

### Port States

```code
open
    An application is actively accepting TCP connections, UDP datagrams or SCTP associations on this port. Finding these is often the primary goal of port scanning. Security-minded people know that each open port is an avenue for attack. Attackers and pen-testers want to exploit the open ports, while administrators try to close or protect them with firewalls without thwarting legitimate users. Open ports are also interesting for non-security scans because they show services available for use on the network.

closed
    A closed port is accessible (it receives and responds to Nmap probe packets), but there is no application listening on it. They can be helpful in showing that a host is up on an IP address (host discovery, or ping scanning), and as part of OS detection. Because closed ports are reachable, it may be worth scanning later in case some open up. Administrators may want to consider blocking such ports with a firewall. Then they would appear in the filtered state, discussed next.

filtered
    Nmap cannot determine whether the port is open because packet filtering prevents its probes from reaching the port. The filtering could be from a dedicated firewall device, router rules, or host-based firewall software. These ports frustrate attackers because they provide so little information. Sometimes they respond with ICMP error messages such as type 3 code 13 (destination unreachable: communication administratively prohibited), but filters that simply drop probes without responding are far more common. This forces Nmap to retry several times just in case the probe was dropped due to network congestion rather than filtering. This slows down the scan dramatically.

unfiltered
    The unfiltered state means that a port is accessible, but Nmap is unable to determine whether it is open or closed. Only the ACK scan, which is used to map firewall rulesets, classifies ports into this state. Scanning unfiltered ports with other scan types such as Window scan, SYN scan, or FIN scan, may help resolve whether the port is open.

open|filtered
    Nmap places ports in this state when it is unable to determine whether a port is open or filtered. This occurs for scan types in which open ports give no response. The lack of response could also mean that a packet filter dropped the probe or any response it elicited. So Nmap does not know for sure whether the port is open or being filtered. The UDP, IP protocol, FIN, NULL, and Xmas scans classify ports this way.

closed|filtered
    This state is used when Nmap is unable to determine whether a port is closed or filtered. It is only used for the IP ID idle scan.
```
