+++
title = "Shells"
description = ""
categories = []
tags = []
weight = 100
+++

## Shells
```bash

# inline python
python -c 'import pty;pty.spawn("/bin/bash")'

# run bash from python
import os
os.system("/bin/bash")

# run bash from python
import pty
pty.spawn("/bin/bash")

```
