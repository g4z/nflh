+++
title = "Passwords"
description = ""
categories = []
tags = []
weight = 100
+++

## Passwords

> Information related to generating and cracking passwords

### Generating

> Ways to generate passwords

#### OpenSSL

> OpenSSL command line tool

```bash
# generate using default `crypt` algorithm
echo -n "password123" | openssl passwd -stdin

# specifically ask for `crypt` algorithm
echo -n "password123" | openssl passwd -stdin -crypt

# generate MD5-based BSD password
echo -n "password123" | openssl passwd -stdin -1

# generate password using Apache variant of the BSD algorithm
echo -n "password123" | openssl passwd -stdin -apr1

# generate a password using a specific salt
echo -n "password123" | openssl passwd -stdin -salt P0t9k -1
```

#### Using /dev/urandom

> Create passwords manually, like a real hacker

```bash
# generate password from random data
head -c 64 /dev/urandom | tr -dc 'a-zA-Z0-9~!@#$%^&*_-'
```

#### John the Ripper

> Generate passwords in dictionary mode and send them to stdout

```bash
# just output passwords using default wordlist
john --stdout --wordlist

# apply default rules to the wordlist and output
john --stdout --wordlist --rules

# specify path to a custom wordlist
john --stdout --wordlist=rockyou.txt

# constrain password length to 2-3 characters
john --stdout:2:3 --wordlist
```

> Generate passwords in incremental mode and send them to stdout

```bash

# default incremental mode
john --stdout --incremental

# constrain password length to 8 characters
john --stdout:8:8 --incremental

# generate Leet-speak format (externals are defined in `john.conf`)
./john --stdout --wordlist --external=Leet

# generate using the ROT13 external
./john --stdout --wordlist --external=Filter_ROT13
```

### Cracking

> Commands for cracking various formats of password

#### John the Ripper

> Active password cracking tool

##### Supported password formats

> Get information about password formats supported by `john`

```bash
# list all supported formats
john --list=formats

# show format details
john --list=format-all-details --format=mysql-sha1
```

##### Common options

| Flag       | Description
|------------|-------------------------------------------------------------
| --wordlist | Path to the wordlist to use
| --fork     | Number of threads to use
| --format   | Specify the password format
| --show     | Show cracked passwords for this file

##### Examples

```bash
# autodetect and crack passwords in ~/hash.txt
# lets john choose the method, wordlist, etc...
john ~/hash.txt

# do the same thing using 2 threads
john --fork 2 ~/hash.txt

# crack an MD5 encoded password stored in ~/hash.txt using ~/rockyou.txt
john --wordlist=rockyou.txt --format=raw-md5 ~/hash.txt

# crack a MySQL password stored in ~/mysql.txt using ~/rockyou.txt
john --format=mysql-sha1 ~/mysql.txt

# show cracked passwords for ~/hash.txt
john --show --format=raw-md5 ~/hash.txt
```

##### Utilities

John comes with various utilities to convert things to a format that john can crack.

```bash
# convert a private key to crackable format
ssh2john.py id_rsa > hash
```

List of some of the John convertors:

- 7z2john.pl
- applenotes2john.py
- bitcoin2john.py
- ecryptfs2john.py
- encfs2john.py
- ethereum2john.py
- filezilla2john.py
- keepass2john
- mongodb2john.js
- mozilla2john.py
- pcap2john.py
- pdf2john.pl
- pem2john.py
- putty2john
- rar2john
- ssh2john.py
- telegram2john.py
- wpapcap2john
- zip2john

### Links

- [rockyou.txt password list](http://downloads.skullsecurity.org/passwords/rockyou.txt.bz2)
