+++
title = "Image Formats"
description = ""
categories = []
tags = []
weight = 100
+++

## Image Formats

### .ova to .qcow2

```bash
# decompress .ova to access the embedded .vmdk image
tar xvf whatever.ova
qemu-img convert -O qcow2 example.vmdk example.qcow2
```

### .vmdk to .qcow2

```bash
qemu-img convert -O qcow2 example.vmdk example.qcow2
```
