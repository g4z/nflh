+++
title = "KVM"
description = ""
categories = []
tags = []
weight = 100
+++

## Kernel Virtual Machine

### VM Management

```bash

# list running vms
virsh list

# list all vms
virsh list --all

# start vm named `vm1`
virsh start vm1

# shutdown vm named `vm1`
virsh shutdown vm1

# get a console for vm named `vm1`
virsh console vm1

# delete a vm
virsh undefine vm1
```

### Networking

```bash
# list defined networks
virsh net-list

# show dhcp info for all vms
virsh net-dhcp-leases default
```

### Import and Export

```bash
# export XMl for a vm
virsh dumpxml vm1 > vm1.xml

# find the path to the image for the xml export
grep "source file" vm1.xml

# import the exported xml on another kvm
virsh define vm1.xml

```
