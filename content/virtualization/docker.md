+++
title = "Docker"
description = ""
categories = []
tags = []
weight = 100
+++

## Docker
```bash

# simpler fix for terminal size detection issue
docker exec -e COLUMNS="`tput cols`" -e LINES="`tput lines`" -ti container bash

# fixes docker terminal size detection issues for current session
$ docker exec -it foo /bin/bash
foo@649fb21d747c:~$ stty size
0 0
foo@649fb21d747c:~$ reset -w
foo@649fb21d747c:~$ stty size
24 80
foo@649fb21d747c:~$ # That was still wrong. Now resize the terminal to get a SIGWINCH.
foo@649fb21d747c:~$ stty size
69 208
foo@649fb21d747c:~$ exit
exit
$ docker exec -it foo /bin/bash # Try it again.
foo@649fb21d747c:~$ stty size
69 208
foo@649fb21d747c:~$ # Doesn't happen anymore for this session.


# docker move image
sudo docker save image1 | gzip > /home/image1.tgz
gunzip /home/image1.tgz
docker load < /home/image1.tar

docker swarm init --advertise-addr 10.175.199.100

docker login registry.gitlab.com -u g4z

docker run -p 8080:8080 -t webgoat/webgoat-7.1

# # lxc exec docker0 -- docker stack deploy -c stack.yml app1
# # docker service create --name stack1 --publish 80:80 --replicas 1 nginx

docker run swarm create
docker run -p 8080:8080 -t webgoat/webgoat-7.1
docker run -d -p 80:8080 swaggerapi/swagger-editor
docker run -d -p 80:80 -p 3306:3306 -e MYSQL_PASS="Test1234" citizenstig/dvwa
docker run --rm -it -v $PWD:/go/src/gitlab.com/g4z/qp qp bash

10113  docker run --privileged --rm -it -v $PWD:/go/src/gitlab.com/g4z/qp -v /dev/snd:/dev/snd -v /dev/shm:/dev/shm -v /etc/machine-id:/etc/machine-id -v /ser/$(id -u)/pulse:/run/user/$(id -u)/pulse -v /var/lib/dbus:/var/lib/dbus -v ~/.pulse:/home/$dockerUsername/.pulse qp /bin/bash

10114  docker run --privileged --rm -it --privileged -v $PWD:/go/src/gitlab.com/g4z/qp -v /dev/snd:/dev/snd -v /dev/shm:/dev/shm -v /etc/machine-id:/etc/d -v /run/user/$(id -u)/pulse:/run/user/0/pulse -v /var/lib/dbus:/var/lib/dbus -v ~/.pulse:/root/.pulse qp /bin/bash

docker run --rm -it -v /tmp/.X11-unix:/tmp/.X11-unix \ # mount the X11 socket
docker run --rm -it -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=unix$DISPLAY --device /dev/snd qp bash
docker run --rm -it -v $PWD:/go/src/gitlab.com/g4z/qp -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=unix$DISPLAY --device /dev/snd qp bash
docker run --rm -it --privileged -v $PWD:/go/src/gitlab.com/g4z/qp -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=unix$DISPLAY --device /dev/snd qp bash
docker run --rm -it -v $PWD:/go/src/gitlab.com/g4z/qpqp bash
docker run -ti --rm -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix qp
docker run -ti --rm -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix qp xeyes
docker run --entrypoint xeyes -ti --rm -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix qp
docker run -ti --rm -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix --entrypoint xeyes qp



10392  docker run --rm -it     --device /dev/snd     --group-add $(getent group audio | cut -d: -f3)     -e PULSE_SERVER=unix:${XDG_RUNTIME_DIR}/pulse/native     -e DISPLAY=unix${DISPLAY}     -v /tmp/.X11-unix:/tmp/.X11-unix:ro     -v ${XDG_RUNTIME_DIR}/pulse/native:${XDG_RUNTIME_DIR}/pulse/native     -v ~/.config/ookie:/root/.config/pulse/cookie     -v $PWD:/go/src/gitlab.com/g4z/qp     --entrypoint "go run /go/src/gitlab.com/g4z/qp/qp/voice"     qp


docker run --entrypoint /bin/bash  -ti --rm qp bash
docker run --entrypoint bash  -ti --rm qp
docker run -ti --rm bingo
docker run -ti --rm bingo bash
docker run --entrypoint bash -ti --rm bingo bash
docker run --entrypoint bash -ti --rm bingo
docker run --entrypoint bash -ti --rm -p 4000:4000 bingo
docker run -ti --rm -p 4000:4000 bingo
docker run -v     $(pwd):/usr/src/app     -p 8080:8080     --name travel_membersite_dev_1     travel_membersite_dev
docker run -v     $(pwd):/usr/src/app     -p 8080:8080     --name travel_membersite_dev_1     travel_membersite_dev
docker run --rm -it php:cli-alpine sh
docker run --rm -it -p80:8888 nginx:alpine


docker login registry.gitlab.com
docker remote ls

docker build -t registry.gitlab.com/g4z/microservice-airports .
docker push registry.gitlab.com/g4z/microservice-airports


```


```
30834  lxc exec docker0 -- docker swarm init --advertise-addr 10.175.199.30
30942  docker swarm
30943  docker swarm leave
30944  docker swarm leave --force
36477  history | grep "docker swarm" | p

```
