+++
title = "Kubernetes"
description = ""
categories = []
tags = []
weight = 100
+++

## Kubernetes
```bash
######## install minikube ########

# dl minikube and kvm driver
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
sudo install minikube-linux-amd64 /usr/local/bin/minikube
curl -LO https://storage.googleapis.com/minikube/releases/latest/docker-machine-driver-kvm2
sudo install docker-machine-driver-kvm2 /usr/local/bin/

# set kvm driver
minikube config set vm-driver kvm2

# show minikube driver
minikube config get vm-driver

# start cluster
minikube start

######## install helm ###########

curl -Lo helm-linux-amd64.tar.gz https://storage.googleapis.com/kubernetes-helm/helm-v2.13.1-linux-amd64.tar.gz
tar -xvf helm-linux-amd64.tar.gz
chmod +x linux-amd64/helm
sudo mv linux-amd64/helm /usr/local/bin/

$ helm init
$ helm repo update

######## commands ########

# get IP of minikube cluster
$ minikube ip

# get cluster status
$ minikube status
minikube status
host: Running
kubelet: Running
apiserver: Running
kubectl: Correctly Configured: pointing to minikube-vm at 192.168.39.204

# get cluster info
$ kubectl cluster-info
Kubernetes master is running at https://192.168.39.204:8443
KubeDNS is running at https://192.168.39.204:8443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

# get IP of a service
minikube service <service> --url

# list pods on cluster
kubectl get pods

# helm install LAMP stack from github.com/helm/charts
helm install --name lamp2 --set service.type=NodePort stable/lamp

# delete it
helm delete lamp2

```
