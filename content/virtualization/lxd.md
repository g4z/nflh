+++
title = "LXD"
description = ""
categories = []
tags = []
weight = 100
+++

## LXD

> The LXD container manager (daemon)

### Images/Repositories

```bash
# list local images
lxc image list

# list remote repositories
lxc remote list

# list images on remote `ubuntu`
lxc image list ubuntu:

# copy a remote image to local
lxc image copy ubuntu:16.04 local:

# create image from stopped container
lxc publish --alias name1 c1

# create image from running container (will be stopped)
lxc publish -f --alias name1 c1
```

### Manage Containers

```bash
# create new container `c1` from image `ubuntu:16.04`
lxc init ubuntu:16.04 c1

# start a container
lxc start c1

# create and start a container
lxc launch ubuntu:16.04 c1

# specify a version
lxc launch images:alpine/edge c1

# stop a container
lxc stop c1

# force delete a container (even while running)
lxc delete -f c1
```

### Privileged Containers and Security Nesting

```bash
# launch container allowing nesting (eg. for running docker)
lxc launch ubuntu:16.04 -c security.nesting=true c1

# launch a privileged container
lxc launch ubuntu:16.04 -c security.privileged=true c1
```

### Console/Exec/Login

```bash
# example login as a user to running container
lxc exec c1 -- sudo --login --user ubuntu

# run a shell command in a container
lxc exec c1 -- sh -c "hostname"

# run a command directly in a container
lxc exec c1 -- apt-get update
```

### Configuration

```bash
# list devices for a container
lxc config device list c1

# show full device configuration for container
lxc config device show c1

# show metadata for a container
lxc config metadata show c1

# set security nesting option
lxc config set c1 security.nesting true

# set privileged option
lxc config set c1 security.privileged true

# ??? set kernel modules (needed this for nesting docker in lxd)
lxc config set c1 linux.kernel_modules ip_tables,ip6_tables,netlink_diag,overlay,nf_nat,br_netfilter,xt_conntrack

# ???disables effects of apparmor
lxc.apparmor.profile=unconfined

# ??? mount options
lxc.mount.auto=proc:rw sys:rw

# ??? setting raw.lxc data
cat <<EOF | lxc config set c1 raw.lxc -
lxc.cgroup.devices.allow=a
lxc.cap.drop=
EOF
```

### Shared Folders

```bash
# add a shared folder
lxc config device add c1 share1 disk source=/local/path path=/container/path

# remove shared folder
lxc config device remove c1 share1
```

### File Transfer

```bash
# push a file to a container
lxc file push /local/path c1/container/path

# push a file to a container
lxc file pull c1/container/path /local/path

# delete a file from the container
lxc file delete c1/container/path

# edit a file in the container
lxc file edit c1/container/path
```

### Networking

```bash
# list defined networks
lxc network list

# create a new network
lxc network create xbr0

# show network information
lxc network show xbr0

# attach a network to a container
lxc network attach xbr0 c1 eth0 eth0

# detach a network from a container
lxc network detach xbr0 c1 eth0

# set the ip address of a container
lxc config device set c1 eth0 ipv4.address 192.168.99.3

# Use `jq` to extract the primary IP address
lxc list --format json c1 | jq .[].state.network.eth0.addresses[0].address
```

### Profiles

```bash
# list all profiles
lxc profile list

# display profile `default`
lxc profile show default

# create a new profile
lxc profile create p1

# edit the profile
lxc profile edit p1

# delete the profile
lxc profile rm p1

# add a root disk to profile
lxc profile device add p1 root disk path=/ pool=default

# add nic to a profile
lxc profile device add p1 eth0 nic name=eth0 nictype=bridged parent=xbr0

# assign a profile to a container
lxc profile assign c1 p1

# get config values from a profile
lxc profile get p1 security.nesting

# get config values from a profile
lxc profile get p1 linux.kernel_modules
```
