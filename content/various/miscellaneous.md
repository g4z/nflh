+++
title = "Miscellaneous"
description = ""
categories = []
tags = []
weight = 100
+++

## Miscellaneous
```bash
# wrap text to column width
fold -w 80 -s source.txt

# examine the codes sent by the keyboard
showkey -a # then hit some keys

# sync target with source by timestamp (add -c for content hash)
rsync -rtvu /path/to/source/ /path/to/target/

# generate random number
shuf -i 1-99999 -n 1

# generate random data
head -c 500 /dev/urandom | tr -dc 'a-zA-Z0-9~!@#$%^&*_-' | fold -w 10

# mount a cryfs encrypted folder
cryfs /path/to/encrypted/folder ~/mnt

# decompress a raw zlib encoded file (no gz headers)
printf "\x1f\x8b\x08\x00\x00\x00\x00\x00" |cat - a.gz | gzip -dc > output

# create and enable a swap file
sudo dd if=/dev/zero of=/swapfile bs=500M count=32
sudo chmod 600 /swapfile
sudo mkswap /swapfile
sudo swapon /swapfile
sudo swapon -s
echo "/swapfile swap swap defaults 0 0" | sudo tee -a /etc/fstab

```
