+++
title = "Text Processing"
description = ""
categories = []
tags = []
weight = 100
+++

## Text Processing

> Operations on textual data

```bash
# example printing columns with awk
echo "this is a test" | awk '{print $4,$3,$2,$1}'
```
