+++
title = "PDF"
description = ""
categories = []
tags = []
weight = 100
+++

## PDF
```bash
# concatenate multi pdfs
gs -q -sPAPERSIZE=letter -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -sOutputFile=Output.pdf 1.pdf 2.pdf ...

# convert txt to ps to pdf
# sudo apt-get install enscript
enscript -p file.ps file.txt
ps2pdf file.ps file.pdf

```
