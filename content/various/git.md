+++
title = "Git"
description = ""
categories = []
tags = []
weight = 100
+++

## Git

> The stupid content tracker

```bash

# perform a shallow clone (ignore history, branches, etc..)
git clone --depth 1 git@gitlab.com:g4z/example.git

# pull everything for a shallow cloned local repo
git fetch --unshallow

# push a local branch to a remote and track it
git push -u origin master

# add a new remote called upstream
git remote add origin git@gitlab.com:g4z/example.git

# update the current origin remote
git remote set-url origin git@gitlab.com:g4z/example.git

# discard all local changes
git reset --hard HEAD

# update last commit if not pushed
git commit --amend

# list commits by author (case sensitive)
git log --author Dan
```
