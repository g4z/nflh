+++
title = "PHP"
description = ""
categories = []
tags = []
weight = 100
+++

## PHP

> Notes about developing in PHP

### Timezone

> Useful information for working with timezones

Sometimes you may need to use the timezone of a city and following the daylight savings times (eg. `America\Toronto`)

Sometimes you may need to use the timezone itself (eg. EST). Using `America/Toronto` will follow EST in the winter but EDT in the summer.

If you need to track EST all the time, you can use the "no DST" options below...

```bash
# Example with Canadian timezones
Newfoundland ...... America/St_Johns
Atlantic .......... America/Halifax
Atlantic no DST ... America/Blanc-Sablon
Eastern ........... America/Toronto
Eastern no DST .... America/Atikokan
Central ........... America/Winnipeg
Central no DST .... America/Regina
Mountain .......... America/Edmonton
Mountain no DST ... America/Creston
Pacific ........... America/Vancouver
````

### Laravel

Capture all SQL queries

```php
// in AppServiceProvider.php :: boot()

\DB::listen(
    function ($query) {
        $bindings = $query->bindings;
        $j = count($bindings);
        for ($i = 0; $i < $j; $i++) {
            if ($bindings[$i] instanceof DateTime) {
                $bindings[$i] = $bindings[$i]->format("'Y-m-d H:i:s'");
            } elseif (is_string($bindings[$i])) {
                $bindings[$i] = sprintf('\'%s\'', $bindings[$i]);
            }
        }
        $msg = vsprintf(
            str_replace(
                ['%', '?'],
                ['%%', '%s'],
                $query->sql
            ),
            $bindings
        );

        \Log::info($msg);
    }
);
```
