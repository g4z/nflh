+++
title = "Python"
description = ""
categories = []
tags = []
weight = 100
+++

## Python

> Notes about developing in Python

### Pipenv

> Python Dev Workflow for Humans

```bash
# create new project using python 3.7
pipenv --python 3.7

# spawns a shell within the virtualenv
pipenv shell

# output bash auto-complete commands
pipenv --completion

# search for a package
pipenv search BeautifulSoup

# install a package
pipenv install BeautifulSoup

# uninstall a package
pipenv uninstall BeautifulSoup

# install all packages specified in pipfile.lock
pipenv sync

# uninstall all packages not specified in pipfile.lock
pipenv clean

# display dep graph for installed packages
pipenv graph

# check your installed dependencies for security vulnerabilities
pipenv check

# spawns a command installed into the virtualenv
pipenv run

# remove project virtualenv (inferred from current directory)
pipenv --rm

# output project home information
pipenv --where

# output python interpreter information
pipenv --py

```

### pip3

```bash
# install package system wide
pip3 install package1

# install packages for current user only
pip3 install --user package1

# force install specific version
pip3 install --force-reinstall package1==2.3

# update a package
pip3 install --upgrade package1
```

### Simple UDP example

```py
from socket import *
cs = socket(AF_INET, SOCK_DGRAM)
cs.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
cs.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)
cs.sendto(b'This is a test', ('255.255.255.255', 55555))
````

