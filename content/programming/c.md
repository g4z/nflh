+++
title = "C"
description = ""
categories = []
tags = []
weight = 100
+++

## C Programming Language

> Notes about developing in C

### Static compile

```bash
# compile a static binary
gcc -static main.c -o main
```

### Compile for x86 on x64

```bash
# install tools for building 32bit binaries
sudo apt install libc6-dev-i386
```

### Cross-compile for ARM

```bash

# install required tools
sudo apt install \
	libc6-dev-armhf-cross \
	gcc-arm-linux-gnueabihf \
	qemu-user \
	uml-utilities

# compile for ARM7 (armhf)
arm-linux-gnueabihf-gcc -static main.c -o main
```
