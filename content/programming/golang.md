+++
title = "Golang"
description = ""
categories = []
tags = []
weight = 100
+++

## Golang

> Behold the gophers

### Cross-compile

- GOARCH: architecture (eg. amd64, 386 or arm)
- GOOS: operating system (eg. linux, darwin or windows)

```bash
# Compile for x64 Linux
GOARCH=amd64 GOOS=linux go build main.go

# Compile for ARM Linux
GOARCH=arm GOOS=linux go build main.go
```

Some possible values for GOOS:

- linux
- windows
- darwin
- js

Some possible values for GOARCH:

- amd64
- arm64
- arm
- 386
- wasm

Look [here](https://github.com/golang/go/blob/master/src/go/build/syslist.go) for a full list of supported GOOS and GOARCH values.

### Debugging

> Debugging using delve

```bash
dlv debug ./bettercap
dlv exec ./bettercap
dlv exec /usr/local/bin/bettercap
dlv run bettercap
```

### Minimise Binaries

```bash
# compress or expand executable files
upx --brute /pth/to/bin

# discard symbols from object files.
strip /path/to/bin

```
