+++
title = "Assembly"
description = ""
categories = []
tags = []
weight = 100
+++

## Assembly Language

> Notes about developing in assembly language

### NASM

> Netwide Assembler, a portable 80x86 assembler

#### Assembling

```bash
# assemble for 32bit
nasm -f elf32 example.asm -o example.o

# assemble for 64bit
nasm -f elf64 example.asm -o example.o
```

#### Linking

```bash
# link for 32bit
ld -m elf_i386 example.o -o example

# link for 64bit
ld -m elf_x86_64 example.o -o example
```

#### Link with the C Standard Library

```bash
# link to c stdlib for 32bit
gcc -no-pie -m32 example.o -o example

# link to c stdlib for 64bit
gcc -no-pie -m64 example.o -o example
```

The `-no-pie` may be Debian specific... i'm not sure. More info [here](https://stackoverflow.com/questions/48071280/nasm-symbol-printf-causes-overflow-in-r-x86-64-pc32-relocation).
