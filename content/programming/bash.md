+++
title = "Bash"
description = ""
categories = []
tags = []
weight = 100
+++

## Bash

> Notes about scripting with /bin/bash

```bash
# example while loop
ls | while read i
    echo $i
done
```
