+++
title = "Redis"
description = ""
categories = []
tags = []
weight = 100
+++

## Redis

> Persistent key-value database

```bash
# connect localhost
redis-cli

# connect remote
redis-cli -h redis.domain.com

# list all keys
echo "keys *" | redis-cli

# flush everything
redis-cli flushall

# get range
echo "lrange queues:default 0 -1" | redis-cli

# show channels
echo "PUBSUB CHANNELS" | redis-cli
```
