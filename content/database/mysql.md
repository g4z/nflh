+++
title = "MySQL"
description = ""
categories = []
tags = []
weight = 100
+++

## MySQL

> MySQL database server

### SQL Queries

> Useful SQL queries and snippets

```bash
# load a filesystem file
SELECT LOAD_FILE("/etc/passwd");

# enable the query log
SET global general_log = 1;

# check last updated time for tables in schema
SELECT TABLE_SCHEMA,TABLE_NAME,UPDATE_TIME FROM information_schema.tables WHERE TABLE_SCHEMA = 'trvl';
```

### Dump and Restore

> Import and export data using mysqldump files

```bash
# load a schema from compressed mysqldump file
zcat dump.sql.gz | mysql -uroot -p schema1
```

### Server Configuration

> Various MySQL server configuration options

```bash
???
```
