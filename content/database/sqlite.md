+++
title = "SQLite"
description = ""
categories = []
tags = []
weight = 100
+++

## SQLite

> SQLite is a C library that implements an SQL database engine

```bash
# connect to a file
sqlite3 db.sqlite

# run a single query
sqlite3 db.sqlite "select * from failed_jobs"

# list tables with `line` style output
sqlite3 --line db.sqlite ".tables"

# show full schema or table schema
.schema
.schema table

```
