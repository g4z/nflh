+++
title = "Cockroach"
description = ""
categories = []
tags = []
weight = 100
+++

## CockroachDB

### Various SQL commands

```bash
# define an auto-incrementing uuid primary key
# CREATE table ....
id UUID DEFAULT uuid_v4()::UUID PRIMARY KEY

# show table schema
SHOW COLUMNS FROM tablename
```
