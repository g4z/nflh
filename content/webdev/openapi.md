+++
title = "OpenAPI"
description = ""
categories = []
tags = []
weight = 100
+++

## OpenAPI

>> Document an API

```bash
# validate/serve/lint AOS3
docker run \
  -v ${PWD}:/project \
  philsturgeon/speccy lint openapi.yml

# openapi-generator-cli
# HTML output sucks right now
# Laravel codegen output may be useful
docker run --rm -v ${PWD}:/local \
    --user $(id -u):$(id -g) \
    openapitools/openapi-generator-cli \
    generate \
    -i /local/openapi.yml \
    -g php-laravel \
    -o /local/generated-code
```
