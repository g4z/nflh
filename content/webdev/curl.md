+++
title = "CURL"
description = ""
categories = []
tags = []
weight = 100
+++

## CURL

```bash

# example POST request (`-d` implies `-X POST`)
curl -d "key=val" http://localhost/

# using a proy (eg. mitmproxy)
HTTP_PROXY=http://localhost:8080 HTTPS_PROXY=http://localhost:8080 \
  curl -k -L http://www.example.com/

# example setting a cookie
curl -X POST 'http://localhost/' \
    -H 'Cookie: PHPSESSID=dasnqiv1oeo8tki4u6u09hmop5; path=/; domain=localhost; Expires=Tue, 19 Jan 2038 03:14:07 GMT;'

# example uploading a file
curl -X POST 'http://localhost/upload' \
    -H 'content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' \
    -F file1=@/path/to/file

# view TLS certification details
curl --insecure -v https://localhost 2>&1 | awk 'BEGIN { cert=0 } /^\* SSL connection/ { cert=1 } /^\*/ { if (cert) print }'
```
