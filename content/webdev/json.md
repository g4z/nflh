+++
title = "JSON"
description = ""
categories = []
tags = []
weight = 100
+++

## JSON

### jq

```bash
cat /tmp/diocane.json | jq
cat /tmp/diocane.json | jq '.'
cat /tmp/diocane.json | jq '.ok'
cat /tmp/diocane.json | jq .data
cat /tmp/diocane.json | jq .ok
cat /tmp/diocane.json | jq '.data.itinerary_data.*.opaque'
cat /tmp/diocane.json | jq '.data.itinerary_data.opaque'
cat /tmp/diocane.json | jq '.data.itinerary_data..opaque'
cat /tmp/diocane.json | jq '.data.itinerary_data.[].opaque'
cat /tmp/diocane.json | jq '.data.itinerary_data.[] | .opaque'
cat /tmp/diocane.json | jq '.data.itinerary_data.[:] | .opaque'
cat /tmp/diocane.json | jq '.data.itinerary_data'
cat /tmp/diocane.json | jq '.data.itinerary_data[].'
cat /tmp/diocane.json | jq '.data.itinerary_data[].opaque'
cat /tmp/diocane.json | jq '.data.itinerary_data[].slice_data.length'
cat /tmp/diocane.json | jq '.data.itinerary_data[].slice_data'
cat /tmp/diocane.json | jq '.data.itinerary_data[].slice_data | length'
```
