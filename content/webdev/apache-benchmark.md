+++
title = "Apache Benchmark"
description = ""
categories = []
tags = []
weight = 100
+++

## Apache Benchmark
```bash
# benchmark local app (see ulimit -Hn)
ab -c 1000 -n 2000 http://localhost:3000/


ab -p stress-payload.json \
    -m POST \
    -T application/json \
    -c 1 \
    -n 1 \
    http://localhost/v1/authenticate

```
