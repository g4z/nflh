+++
title = "Images"
description = ""
categories = []
tags = []
weight = 100
+++

## Images

> Various commands related to image processing

### Format Conversion

```bash
# convert JPEG to PNG with image magick
convert input.jpg output.png

# specify the resample quality
convert input.jpg -quality 100 output.png

```

### Image Operations

```bash
# resize an image to 640x480 pixels
convert input.png -resize 640x480 output.png

# resize an image to a percentage of the original
convert input.png -resize 50% output.png
```

### Programmatic Image Creation

```bash
# example creating a new image
convert canvas:none \
    -size 320x85 \
    -font Bookman-DemiItalic \
    -pointsize 72 \
    -draw "text 25,60 'Hello'" \
    -channel RGBA \
    -blur 0x6 \
    -fill darkred \
    -stroke magenta \
    -draw "text 20,55 'Hello'" \
    output.png
```

### Win32 icon files (.ico)

```bash
# list files embedded in a favicon
icotool -l example.ico

# extract images from ico file
icotool -x example.ico

# create ico file from png
icotool -c -o example.ico example.png
```

### Links

- [ImageMagick Convert Help](https://imagemagick.org/script/convert.php)
- [ImageMagick Usage Examples](https://imagemagick.org/Usage/)