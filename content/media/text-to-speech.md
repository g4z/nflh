+++
title = "Text to Speech"
description = ""
categories = []
tags = []
weight = 100
+++

## Text to Speech

> Various text-to-speech related commands

### Espeak

> eSpeak is a software speech synthesizer for English, and some other languages

```bash
# list installed voices
ls /usr/lib/x86_64-linux-gnu/espeak-ng-data/voices/mb/ \
    /usr/lib/x86_64-linux-gnu/espeak-ng-data/voices/\!v/

# with default settings
espeak-ng "hello world"

# pipe text to stdin
echo "hello world" | espeak-ng

# use a specific voice
echo "hello world" | espeak-ng -v Denis

# specify speech speed (words-per-minute - default 175)
echo "hello world" | espeak-ng -s 140

# adjust pitch (0-99 - default 50)
echo "hello world" | espeak-ng -p 30

# speak italian
echo "ciao al mondo" | espeak-ng -v mb-it3

```
