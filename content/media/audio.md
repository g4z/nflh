+++
title = "Audio"
description = ""
categories = []
tags = []
weight = 100
+++

## Audio

> Various commands related to sound/audio

## Jackd

```
# install jackd and pulseaudio module
apt-get install qjackctl pulseaudio-module-jack
# now configure qjackctl to run the following command after startup
# "Setup..." > "Options" > "Execute script after Startup"
# pactl set-default-sink jack_out
```

### Amixer

> Command-line mixer for ALSA soundcard driver

```bash

# change playback volume using PulseAudio
amixer -D pulse sset Master 5%+ # Increase volume by 5%
amixer -D pulse sset Master 5%- # Decrease volume by 5%
amixer -D pulse sset Master 50% # Set volume to 50%

# toggle mute/unmute of the mic input
amixer set Capture toggle
```
