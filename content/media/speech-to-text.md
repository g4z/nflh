+++
title = "Speech to Text"
description = ""
categories = []
tags = []
weight = 100
+++

## Speech to Text

> Various speech-to-text related information

### PocketSphinx

> A lightweight speech recognition engine

- specifically tuned for handheld and mobile devices
- though it works equally well on the desktop

#### Cross-compile for ARM

```bash

# install these requirements
sudo apt install \
	libsndfile1-dev \
	libsamplerate0-dev

# cross-compile Sphinxbase for ARM7 (armhf)
./configure --without-python \
	--host=arm-linux-gnueabihf \
	--build=x86_64-linux-gnu \
	--target=arm-linux-gnueabihf

make && make install

```

#### Links

- [github.com/cmusphinx/sphinxbase](https://github.com/cmusphinx/sphinxbase)
- [github.com/cmusphinx/pocketsphinx](https://github.com/cmusphinx/pocketsphinx)
