+++
title = "Home"
chapter = false
+++

## Notes for Linux Hackers

> Some useful commands, code and notes for working with Linux

This website is a collection of various commands, notes and code snippets that I wanted in a single place for reference.

#### Contents

{{% children %}}
