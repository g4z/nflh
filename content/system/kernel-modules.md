+++
title = "Kernel Modules"
description = ""
categories = []
tags = []
weight = 100
+++

## Kernel Modules

```bash
lsmod | grep br_netf
insmod br_netfilter
modinfo br_netfilter
insmod /lib/modules/4.15.0-46-generic/kernel/net/bridge/br_netfilter.ko
sudo insmod /lib/modules/4.15.0-46-generic/kernel/net/bridge/br_netfilter.ko
```
