+++
title = "Processes"
description = ""
categories = []
tags = []
weight = 100
+++

## Processes
```bash
kill -l # list available signals
kill -15 SIGTERM (default)
kill -2
kill -1
kill -9 -> kill without mercy


# jobs
$sleep 999
^Z
$ jobs
[1]+  Stopped                 sleep 33
$ fg %1
sleep 33
$
```
