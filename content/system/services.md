+++
title = "Services"
description = ""
categories = []
tags = []
weight = 100
+++

## Services

> Systemd: System and service manager

### Overview

Systemd has the concept of a "unit". Some units are services and they use a name suffix of ".service" within systemd (eg nginx.service). Mostly, you can leave off the ".service" part.

### Listing unit information

```bash
# list active units (default action)
systemctl list-units

# list active units is the default action
systemctl

# list all units
systemctl --all

# filter by type (show only service units)
systemctl --type service
```

### Starting and stopping services

```bash
# start a service
sudo systemctl start example.service

# stop a service
sudo systemctl stop example.service

# restart a service
sudo systemctl restart example.service

# reload service config (if service supports it)
sudo systemctl reload example.service

# try a reload and fallback to a restart
sudo systemctl reload-or-restart example.service
```

### Enable or disable services

```bash
# enable a service on system startup
sudo systemctl enable example.service

# disable a service on system startup
sudo systemctl disable example.service

# check if a service is enabled
systemctl is-enabled example.service
```

## Check service status

```bash
# show status information about a running service
systemctl status example.service

# check if a services is running
systemctl is-active example.service

# check if service is in the failed state
systemctl is-failed example.service
```

### Links

- [Digital Ocean Tutorial](https://www.digitalocean.com/community/tutorials/how-to-use-systemctl-to-manage-systemd-services-and-units)