+++
title = "Tweaks"
description = ""
categories = []
tags = []
weight = 100
+++

## Tweaks
```bash

# Disable auto-upgrades
sudo nano /etc/apt/apt.conf.d/20auto-upgrades
# Once you have the file opened, switch off the Update-Package-Lists directive from 1 to 0 as shown below on Line 1:
# APT::Periodic::Update-Package-Lists "1";
# APT::Periodic::Unattended-Upgrade "1";
# replace 1 with 0 to disable

```
