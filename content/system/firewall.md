+++
title = "Firewall"
description = ""
categories = []
tags = []
weight = 100
+++

## Firewall

### UFW

```bash
# enable
sudo ufw enable

# ufw status
sudo ufw status

# ufw detailed output
sudo ufw status verbose

# who raw
sudo ufw show raw

# add rule for kde-connect
sudo ufw allow proto tcp to any port 1714:1764
```

### IPTables

```bash

# list iptables rules
sudo iptables --list

# show a chain
sudo iptables -L FORWARD


```
