+++
title = "Logging"
description = ""
categories = []
tags = []
weight = 100
+++

## System Logging

> Getting system logs using journalctl

```bash
# follow/watch the logs
journalctl -f

# list all messages
journalctl

# output syslog format
journalctl -o short

# filter by service
journalctl -u nginx.service

# filter by time ranges
journalctl --since "2017-01-01 00:00:00" --until "2017-01-02 00:00:00"

# show last 50 messages
journalctl -n 50

# list boot messages from the last boot
journalctl -b -1q

# list system boots
journalctl --list-boots

# output as json data
journalctl -o json-pretty

# show only info and worst messages
journalctl -p "info"
```
