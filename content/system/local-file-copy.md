+++
title = "Local File Copy"
description = ""
categories = []
tags = []
weight = 100
+++

## Local File Copy

> Commands for copy files locally

### dd

> Convert and copy a file

```bash
# copy a file
dd if=src of=dst

# copy a file skipping first 3 bytes (using 1 byte blocks)
dd if=src bs=1 skip=3 of=dst
```
