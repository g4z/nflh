+++
title = "Package Management"
description = ""
categories = []
tags = []
weight = 100
+++

## Package Management

### APK Packages (Alpine Linux)

```bash
# [alpine] list installed packages
apk -vv info | sort
```

### APT Packages (Debian-based Linux)

```bash
# get list of installed files for package
dpkg -L <package>

# show which packages provide "php.ini"
dpkg -S <file>

# reconfigure a deb package
sudo dpkg-reconfigure <package>

# show package information
apt show <package>

# fix broken packages
sudo apt --fix-broken install

# install local deb file
sudo dpkg -i example.deb

```

### SNAP Packages

```bash
# search for snap packages
snap search <keyword>

# update snap packages
snap refresh

# list installed packages
snap list

# install snap package
snap install <package>

# remove snap package
snap remove <package>
```
