+++
title = "Archiving"
description = ""
categories = []
tags = []
weight = 100
+++

## Archiving

> Archiving the things and making them more compact too

### 7zip

> A file archiver with high compression ratio format

```bash
# list files in an archive
7z l example.7z

# extract files in flat structure to ./example/
7z e example.7z

# extract files with full paths
7z x example.7z
```
