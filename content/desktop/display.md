+++
title = "Display"
description = ""
categories = []
tags = []
weight = 100
+++

## Display

> Various commands for dealing with display monitors

```bash
xrandr --output HDMI-1 --mode 1920x1080 --primary --output eDP-1 --off
```
