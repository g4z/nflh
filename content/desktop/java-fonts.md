+++
title = "Miscellaneous"
description = ""
categories = []
tags = []
weight = 100
+++

## Miscellaneous

> Various useful commands for Linux desktop

```bash
# fix ugly (non anti-aliased) fonts in java applications.
# append this `-D` param to the app's startup command
java ... -Dawt.useSystemAAFontSettings=on ...
```
