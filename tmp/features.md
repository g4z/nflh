+++
title = "Features"
description = ""
categories = []
tags = []
weight = 120
+++

## Features

This is an expandable codeblock...

{{%expand%}}
```go
package main

import "net/http"

type Context struct {
	GetEnvironment func() *Environment
	GetRequest     func() *Request
	GetServer      func() *Server
}

func NewContext(r *http.Request) *Context {
	return &Context{
		GetEnvironment: func() *Environment { return NewEnvironment() },
		GetRequest:     func() *Request { return NewRequest(r) },
		GetServer:      func() *Server { return NewServer() },
	}
}
```
{{% /expand%}}

Here is an example codeblock for `bash` shell...

```bash
echo "password" > my.lst
john --wordlist=my.lst --stdout --rules
```

### More Information

This is an example with javascript...

{{% notice warning %}}
An warning disclaimer
{{% /notice %}}

```js
require("common.js")

engine.Run = function() {

    // Test call function from common.js
    console.log(MyCommonFunction())

    handler = engine.NewEventHandler()
    handler.on("event1", function() {
        console.log("event1 triggered")
    })
    handler.emit("event1")

    return {
        "id": "widget1",
        "name": "widget1",
        "cost": "79.99"
    }
}
```

## Diagram Examples

For more information, go here https://learn.netlify.com/en/shortcodes/mermaid/

{{<mermaid align="left">}}
graph LR;
    A[Hard edge] -->|Link text| B(Round edge)
    B --> C{Decision}
    C -->|One| D[Result one]
    C -->|Two| E[Result two]
{{< /mermaid >}}

{{% notice tip %}}
A tip disclaimer
{{% /notice %}}

{{<mermaid align="left">}}
sequenceDiagram
    participant Alice
    participant Bob
    Alice->>John: Hello John, how are you?
    loop Healthcheck
        John->John: Fight against hypochondria
    end
    Note right of John: Rational thoughts <br/>prevail...
    John-->Alice: Great!
    John->Bob: How about you?
    Bob-->John: Jolly good!
{{< /mermaid >}}

### Gantt Chart

{{<mermaid align="left">}}
gantt
        dateFormat  YYYY-MM-DD
        title Adding GANTT diagram functionality to mermaid
        section A section
        Completed task            :done,    des1, 2014-01-06,2014-01-08
        Active task               :active,  des2, 2014-01-09, 3d
        Future task               :         des3, after des2, 5d
        Future task2               :         des4, after des3, 5d
        section Critical tasks
        Completed task in the critical line :crit, done, 2014-01-06,24h
        Implement parser and jison          :crit, done, after des1, 2d
        Create tests for parser             :crit, active, 3d
        Future task in critical line        :crit, 5d
        Create tests for renderer           :2d
        Add to mermaid                      :1d
{{< /mermaid >}}
