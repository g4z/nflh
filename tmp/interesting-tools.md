
===

golang (cross-compile) / c / asm
• logrus
• otto
• packr
• gopacket
• sdl/raylib
• cobra
• viper

arm

embedded/electronics/arduino/microchip
sdr/radio

mapping/gps/nmea/etc..





DNS Enumeration Script (py)
https://github.com/darkoperator/dnsrecon

A script to extract subdomains/emails for a given domain using SSL/TLS certificate dataset on Censys
https://github.com/yamakira/censys-enumeration

A victims'-profile-based wordlist generating tool for social engineers and security researchers!
https://github.com/tch1001/pwdlogy

Exploitation Framework for Embedded Devices
https://github.com/threat9/routersploit

A scanning tool for scraping hostnames from SSL certificates.
https://github.com/cheetz/sslScrape

learns

Big O notation

grid distance (src/gitlab.com/g4z/try-golang/golang-prog-challenge-1/closest-points)

barcode
anti-theft tags
tensorflow (Intel Neural Computer Sick/OpenVINO, Google Edge TPU)
ODB2, CAN BUS

::: golang

gopacket
gaming in golang with sdl
io.Reader,io.Writer,Multiwriter,PipeRW,etc..
debug (delve)
tracer
concurrency (channels/mutex/semaphore)
context

::: embedded

simulIDE
Fritzing
ArduinoIDE

UART

::: reverse

IDA
radare
ghidra

::: various

::: SDR

GQRX
GNU Radio (GRC)
HackRF (300€)


