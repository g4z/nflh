+++
title = "Qemu/PA Fix"
description = ""
categories = []
tags = []
weight = 100
+++

## Qemu + PulseAudio Fix

```bash
qemu/pa fix

A solution for pulseaudio: pa_context_connect() failed with QEMU (self.linux_gaming)

submitted 1 year ago * by deeznutznutznutz

TL;DR See https://wiki.archlinux.org/index.php/PulseAudio/Configuration#Connection_.26_authentication. Root needs to know where the pa socket is, and it needs the cookie to authenticate.

Those of you setting up QEMU with PCI passthrough for gaming with Windows in a VM while booted into Linux may ...lets be honest; probably have had issues setting up audio.

When I tried running it I had the common error:
pulseaudio: pa_context_connect() failed
pulseaudio: Reason: Connection refused
pulseaudio: Failed to initialize PA contextaudio: Could not init pa audio driver

I spent a long time searching and trying different things before determining my issue (and I expect it to be common), came from needing to run qemu-system-x86_64 as root while pulseaudio's daemon was run as my user. Because of this, root is unable to find the pa socket for my user(by default at /run/user/$ID/pulse/native. If you only have one user $ID is probably 1000. The root user will also need access to the cookie generated for your user. Mine was located at ~/.config/pulse/cookie.

This is a simple fix:
Copy your cookie from ~/.config/pulse/cookie to /root/.config/pulse/cookie
Add a line to /root/.config/pulse/client.conf (You may want to copy the global conf file from /etc/pulse/client.conf if it doesn't exist):
default-server = unix:/run/user/$ID/pulse/native
Remember $ID is the id of your user (or whichever user is running the pulseaudio daemon that you want to share). If you only have one user $ID is probably 1000.

A quick test to see if this your problem would be running pulseaudio as root before running your vm:
$ sudo pulseaudio -D
If this makes the pa_context_connect() error go away then that's probably the issue. This isn't a fix though if you still want to use pulseaudio as your user. My system ended up fighting over the same audio sinks when I tried this.

The real solution is pointing root to the pa socket and cookie used by your user.

Hope this helps anyone else who ran into this hurdle. Best of luck!

```
