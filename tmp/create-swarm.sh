
#docker run --privileged hello-world
i=0
# prepare containers
# for i in {0..2}
# do
    lxc init ubuntu:18.04 docker$i
    lxc config set docker$i security.nesting true
    # lxc config set docker$i security.privileged true
    # lxc config set docker$i linux.kernel_modules ip_tables,ip6_tables,netlink_diag,overlay,nf_nat,br_netfilter,xt_conntrack
# lxc.apparmor.profile=unconfined
# lxc.mount.auto=proc:rw sys:rw
#     cat <<EOF | lxc config set docker$i raw.lxc -
# lxc.cgroup.devices.allow=a
# lxc.cap.drop=
# EOF

    lxc network attach lxdbr0 docker$i eth0 eth0
    lxc config device set docker$i eth0 ipv4.address 10.175.199.10$i
    lxc start docker$i
# done

# devices:
#   aadisable:
#     path: /sys/module/nf_conntrack/parameters/hashsize
#     source: /dev/null
#     type: disk
#   aadisable1:
#     path: /sys/module/apparmor/parameters/enabled
#     source: /dev/null
#     type: disk



# # wait (for lxd networking)
sleep 10

# PACKAGES="ca-certificates libvirt-bin qemu-kvm docker.io make htop vim tree jq git curl"
PACKAGES="ca-certificates docker.io make htop vim tree jq git curl"

# # configure the hosts
# # for i in {0..2}
# # do
#     lxc exec docker$i -- ping -c1 google.com
    lxc exec docker$i -- apt-get update
    lxc exec docker$i -- apt-get upgrade -y $PACKAGES
#     # lxc exec docker$i -- sh -c "curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -"
#     # lxc exec docker$i -- add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
#     # lxc exec docker$i -- apt-get install -y docker-ce docker-ce-cli containerd.io
    lxc exec docker$i -- usermod -aG docker ubuntu
    lxc restart docker$i
# # done

#configure docker swarm
# CMD=$(lxc exec docker0 -- docker swarm init --advertise-addr 10.175.199.100 | grep "docker swarm join")
# echo $CMD
# lxc exec docker1 -- $CMD
# lxc exec docker2 -- $CMD

# #configure gitlab access
# # docker login registry.gitlab.com -u g4z
# # rW8LuU-sGdDjsd1gESx4


# ######## testing######################



# # sudo insmod /lib/modules/4.15.0-46-generic/kernel/net/netfilter/ipvs/ip_vs.ko

# # setup repo access
# # lxc exec docker0 -- sudo --login --user ubuntu ssh-keygen -t rsa -f /home/ubuntu/.ssh/docker0.key
# # lxc exec docker0 -- sudo --login --user ubuntu cat /home/ubuntu/.ssh/docker0.key

# # lxc file push docker-compose.yml docker0/home/ubuntu/docker-compose.yml
# # lxc exec docker0 -- docker stack deploy -c stack.yml app1
# # docker service create --name stack1 --publish 80:80 --replicas 1 nginx

