+++
title = "What is it?"
description = ""
categories = []
tags = []
weight = 110
+++

## What is it?

> Remember that you can use blockquotes and they look like this

Discover what this Hugo theme is all about and the core-concepts behind it.

Table are pretty cool and look like this...

{{% notice info %}}
An information disclaimer
{{% /notice %}}

| Option | Description |
| ------ | ----------- |
| data   | path to data files to supply the data that will be passed into templates. |
| engine | engine to be used for processing templates. Handlebars is the default. |
| ext    | extension to be used for dest files. |

Click below to do nothing.

{{% button href="javascript:alert('Did nothing!')" icon="fas fa-download" %}}Do Nothing{{% /button %}}

{{% notice note %}}
A notice disclaimer
{{% /notice %}}

{{--%attachments style="blue" title="Related files" pattern=".*"/%--}}
