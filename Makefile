
dev:
	@hugo server --buildDrafts --disableFastRender

build:
	@hugo

# theme-install:
# 	git submodule add --depth 1 https://github.com/calintat/minimal.git themes/minimal
# 	git submodule init
# 	git submodule update

# theme-update:
# 	git submodule update --remote themes/minimal
